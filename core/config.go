package core

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Version      string `yaml:"version"`
	Host         string `yaml:"host"`
	Port         int32  `yaml:"port"`
	CookieName   string `yaml:"cookie_name"`
	CookieSecure bool   `yaml:"cookie_secure"`
	CookieDomain string `yaml:"cookie_domain"`
	*JWTConfig   `yaml:"jwt_config"`
	*OTPConfig   `yaml:"otp_config"`
}

func (cfg Config) Save(path string) (err error) {
	cfgByteArr, err := yaml.Marshal(cfg)
	if err != nil {
		return
	}

	err = ioutil.WriteFile(path, cfgByteArr, 0600)
	return
}

func (cfg *Config) Read(path string) (err error) {
	cfgStr, err := ioutil.ReadFile(path)
	if err != nil {
		return
	}

	err = yaml.Unmarshal(cfgStr, cfg)
	return
}

func (cfg Config) Validate() bool {
	if cfg.JWTKey == "" || cfg.TOTPSecret == "" {
		return false
	}

	return true
}
