package core

import (
	"log"

	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
)

type OTPConfig struct {
	TOTPSecret string `yaml:"totp_secret"`
	TOTPUser   string `yaml:"totp_user"`
}

func GenerateTOTP(cfg *OTPConfig, accName string) *otp.Key {
	key, err := totp.Generate(
		totp.GenerateOpts{
			Issuer:      "Auth-Gateway",
			AccountName: accName,
		})
	if err != nil {
		log.Fatal(err)
	}

	cfg.TOTPUser = accName
	cfg.TOTPSecret = key.Secret()

	return key
}

func ValidatePassword(passwd string, cfg *OTPConfig) bool {
	return totp.Validate(passwd, cfg.TOTPSecret)
}
