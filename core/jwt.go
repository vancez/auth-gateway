package core

import (
	"auth_gateway/utils"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Status uint8

const (
	Invalid Status = iota
	Valid
	Renew
)

type JWTConfig struct {
	JWTKey      string        `yaml:"jwt_key"`
	JWTDuration time.Duration `yaml:"jwt_duration"`
	JWTRenew    time.Duration `yaml:"jwt_renew"`
	JWTLeeway   int64         `yaml:"jwt_leeway"`
}

type StandardClaimsWithLeeway struct {
	*jwt.StandardClaims
	JWTLeeway int64 `json:"-"`
}

func (c StandardClaimsWithLeeway) Valid() error {
	vErr := new(jwt.ValidationError)
	now := time.Now().Unix()

	if c.VerifyExpiresAt(now-c.JWTLeeway, true) == false {
		delta := time.Unix(now, 0).Sub(time.Unix(c.ExpiresAt, 0))
		vErr.Inner = fmt.Errorf("token is expired by %v", delta)
		vErr.Errors |= jwt.ValidationErrorExpired
	}

	if vErr.Errors == 0 {
		return nil
	}

	return vErr
}

func GenerateJWT(cfg *JWTConfig) string {
	currentTime := time.Now()

	payload := StandardClaimsWithLeeway{StandardClaims: &jwt.StandardClaims{}}
	payload.IssuedAt = currentTime.Unix()
	payload.ExpiresAt = currentTime.Add(cfg.JWTDuration).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &payload)

	tokenString, err := token.SignedString([]byte(cfg.JWTKey))
	if err != nil {
		return ""
	}

	return tokenString
}

func ValidateJWT(tokenString string, cfg *JWTConfig) Status {
	parser := jwt.Parser{ValidMethods: []string{"HS256"}, UseJSONNumber: true}

	token, err := parser.ParseWithClaims(tokenString, &StandardClaimsWithLeeway{
		&jwt.StandardClaims{},
		cfg.JWTLeeway,
	},
		func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}

			return []byte(cfg.JWTKey), nil
		})
	if err != nil {
		return Invalid
	}

	if claim, ok := token.Claims.(*StandardClaimsWithLeeway); ok && token.Valid {
		if time.Now().Add(cfg.JWTRenew).After(time.Unix(claim.ExpiresAt, 0)) {
			return Renew
		}
		return Valid
	}

	return Invalid
}

func GenerateJWTKey(cfg *JWTConfig) {
	cfg.JWTKey = utils.RandStringBytesMaskImprSrcUnsafe(32)
}
