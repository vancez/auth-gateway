all: amd64 aarch64

amd64:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags '-s -w --extldflags "-static -fpic"' -o output/auth-gateway.amd64 main.go

aarch64:
	CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -ldflags '-s -w --extldflags "-static -fpic"' -o output/auth-gateway.aarch64 main.go

.PHONY: clean
clean:
	rm -f output/*
