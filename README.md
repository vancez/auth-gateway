# Configuration file (auth.yml)
```
version: 4fe0b0caf8c222ea7bdba7985327ad85
host: 127.0.0.1
port: 10000
cookie_name: AUTH_GATEWAY
cookie_secure: true
cookie_domain: ""
jwt_config:
  jwt_key: BJ4TmxedEorPdNFkXqrTOuffORth0YnP
  jwt_duration: 20m0s
  jwt_renew: 5m0s
  jwt_leeway: 10
otp_config:
  totp_secret: WS4HFD2ZQ2IIHVN2S3SW5TJBU7NKZXUW
  totp_user: testu
```

# Nginx configuration (example.conf)
```
...
location /protected {
    ...
    auth_request /auth;
    auth_request_set $auth_resp_cookie $upstream_http_set_cookie;
    add_header Set-Cookie $auth_resp_cookie;
    ...
}

location = /auth {
    internal;
    proxy_pass http://127.0.0.1:10000/;
    proxy_pass_request_body off;
    proxy_set_header Content-Length "";
    proxy_set_header Host $host;
}
...
```
