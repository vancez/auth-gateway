package main

import (
	"auth_gateway/core"
	"auth_gateway/utils"
	"crypto/md5"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	initConfig  string
	configFile  string
	newTOTPUser string
	newJWTKey   bool
	printConfig bool
	cfg         *core.Config
)

func genJWT() string {
	return core.GenerateJWT(cfg.JWTConfig)
}

func validateJWT(tokenString string) core.Status {
	return core.ValidateJWT(tokenString, cfg.JWTConfig)
}

func validatePassword(passwd string) bool {
	return core.ValidatePassword(passwd, cfg.OTPConfig)
}

func genCookie() *http.Cookie {
	return &http.Cookie{
		Name:     cfg.CookieName,
		Value:    genJWT(),
		Path:     "/",
		HttpOnly: true,
		Secure:   cfg.CookieSecure,
		Domain:   cfg.CookieDomain,
	}
}

func authHandler(w http.ResponseWriter, r *http.Request) {
	if cookie, err := r.Cookie(cfg.CookieName); err == nil {
		switch validateJWT(cookie.Value) {
		case core.Renew:
			http.SetCookie(w, genCookie())
			fallthrough
		case core.Valid:
			w.Write([]byte("jwt ok"))
			return
		}
	}

	if _, password, ok := r.BasicAuth(); ok && validatePassword(password) {
		http.SetCookie(w, genCookie())
		w.Write([]byte("ok"))
		return
	}

	abort401(w, r)
}

func abort401(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("WWW-Authenticate", "Basic realm="+r.Host)
	w.WriteHeader(http.StatusUnauthorized)
}

func parseConfigFile() {
	err := cfg.Read(configFile)
	if err != nil {
		log.Fatal(err)
	}
}

func saveConfigFile() {
	err := cfg.Save(configFile)
	if err != nil {
		log.Fatal(err)
	}
}

func validateConfig() {
	if !cfg.Validate() {
		log.Fatal("Invalid format for configuration file")
	}
}

func initConfigFile() {
	core.GenerateJWTKey(cfg.JWTConfig)
	core.GenerateTOTP(cfg.OTPConfig, initConfig)
	cfg.Version = getVersion()
	saveConfigFile()
	printQRCode()
}

func printQRCode() {
	url := fmt.Sprintf("otpauth://totp/Auth-Gateway:%s?algorithm=SHA1&digits=6&issuer=Auth-Gateway&period=30&secret=%s", cfg.TOTPUser, cfg.TOTPSecret)
	utils.PrintQRCode(url)
}

func getVersion() string {
	exePath, _ := os.Executable()
	f, err := os.Open(exePath)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}

	return hex.EncodeToString(h.Sum(nil))
}

func init() {
	flag.StringVar(&configFile, "config", "", "configuration `file`")
	flag.StringVar(&newTOTPUser, "new-totp", "", "generate TOTP secret with `username`")
	flag.BoolVar(&newJWTKey, "new-jwt", false, "generate JWT key")
	flag.StringVar(&initConfig, "init", "", "initial configuration file with `username`")
	flag.BoolVar(&printConfig, "print", false, "print QRCode")

	cfg = &core.Config{
		Version:      "",
		Host:         "127.0.0.1",
		Port:         10000,
		CookieName:   "AUTH_GATEWAY",
		CookieSecure: true,
		CookieDomain: "",
		JWTConfig: &core.JWTConfig{
			JWTDuration: time.Minute * 20,
			JWTRenew:    time.Minute * 5,
			JWTLeeway:   10,
		},
		OTPConfig: &core.OTPConfig{},
	}
}

func main() {
	flag.Parse()
	if flag.NArg() != 0 || configFile == "" {
		flag.Usage()
		os.Exit(1)
	}

	if initConfig != "" {
		initConfigFile()
		os.Exit(0)
	}

	parseConfigFile()

	if newTOTPUser != "" {
		core.GenerateTOTP(cfg.OTPConfig, newTOTPUser)
		saveConfigFile()
		printQRCode()
	}

	if newJWTKey {
		core.GenerateJWTKey(cfg.JWTConfig)
		saveConfigFile()
	}

	if printConfig {
		printQRCode()
	}

	if len(os.Args) > 3 {
		os.Exit(0)
	}

	validateConfig()

	curVersion := getVersion()
	if curVersion != cfg.Version {
		cfg.Version = curVersion
		saveConfigFile()
	}

	http.HandleFunc("/", authHandler)
	fmt.Printf("Serving on %[1]v port %[2]v (http://%[1]v:%[2]v/) ...\n", cfg.Host, cfg.Port)
	http.ListenAndServe(fmt.Sprintf("%v:%v", cfg.Host, cfg.Port), nil)
}
