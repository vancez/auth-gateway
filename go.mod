module auth_gateway

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/mdp/qrterminal/v3 v3.0.0
	github.com/pquerna/otp v1.2.0
	gopkg.in/yaml.v2 v2.2.8
)
